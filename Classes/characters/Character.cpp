#include "Character.h"

Character::Character(std::string name, Sprite *sprite)
{
	this->id = 0;
	this->name = name;
	this->sprite = sprite;
}

Character::Character(int id, std::string name, Sprite *sprite)
{
	this->id = id;
	this->name = name;
	this->sprite = sprite;
}

Character::~Character()
{
	delete(this->sprite);
}

Sprite* Character::getSprite()
{
	return this->sprite;
}

void Character::setSprite(Sprite *sprite)
{
	this->sprite = sprite;
}
