#include "AppPokemon.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "proj.win32/maps/MapManager.h"
#include "proj.win32/scene/SceneManager.h"
#include "proj.win32/characters/Trainer.h"
#include "proj.win32/tools/Debug.h"
#include "proj.win32/pokemon/Pokemon.h"
#include "proj.win32/ui/UIManager.h"

USING_NS_CC;

using namespace cocostudio::timeline;

Scene* AppPokemon::createScene()
{
    // 'scene' is an autorelease object
    auto scene = SceneManager::create();
	//scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
	
    // 'layer' is an autorelease object
    auto layer = AppPokemon::create();
	layer->setName("Game");
	// add layer as a child to scene
	scene->addChild(layer, 1);

	auto uiManager = UIManager::create();
	uiManager->setName("UI_Manager");
	//add the UI layer
	scene->addChild(uiManager, 2);
	
	// return the scene
	return scene;
}     

// on "init" you need to initialize your instance
bool AppPokemon::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }

    auto rootNode = CSLoader::createNode("MainScene.csb");
    addChild(rootNode);

	return true;
}

// game start here
void AppPokemon::onEnter()
{
	//call parent
	Layer::onEnter();

	//create map
	auto map = new MapManager("town2.tmx");
	auto mapInformation = map->getMapInformation();
	auto playerStart = mapInformation->getObjectGroup("Events")->getObject("PLAYER_START");

	//create player
 	auto trainer = new Trainer("Blue", Sprite::create("characters/trainers/trainer2.png", Rect(0, 0, 32, 32)));
 	trainer->setPosition(Vec2(playerStart["x"].asFloat() + (playerStart["width"].asFloat() / 2), playerStart["y"].asFloat() + (playerStart["height"].asFloat() / 2)));
	//this->addChild(trainer, TILE_PLAYER);

	trainer->addPokemonToBelt(Pokemon("Charmander", 0,0,0,0,0,0,0,0));
	trainer->addPokemonToBelt(Pokemon("Bulbasaur", 0,0,0,0,0,0,0,0));
	trainer->addPokemonToBelt(Pokemon("Squirtle", 0,0,0,0,0,0,0,0));

	MapManager::addPlayerToTheMap(trainer);
}
